FROM golang:latest

WORKDIR /app

COPY helloworld/go.mod .
COPY helloworld/login.gtpl .
COPY helloworld/main.go .

RUN go build -o hello-world

EXPOSE 8080

CMD ["./hell-oworld"]