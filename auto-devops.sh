#!/bin/bash

function init_build() {
  #unset all group level proxy defines
  unset no_proxy
  unset NO_PROXY
  unset http_proxy
  unset https_proxy
  unset HTTPS_PROXY
  unset HTTP_PROXY
  #default push to JPE
#   export CI_APPLICATION_REPOSTIORY_NAME="$TENANT_NAME-$NS_ENV/$DOCKER_NAME"
#   export CI_JPE1_APPLICATION_REPOSITORY="$CI_JPE2_REGISTRY/${CI_APPLICATION_REPOSTIORY_NAME}"
  export CI_COMMIT_SHORT_SHA="${CI_COMMIT_SHA:0:8}"
  export CI_APPLICATION_TAG="${CI_COMMIT_SHORT_SHA}"
#   export CI_CONTAINER_NAME="ci_job_build_${CI_JOB_ID}"
}

function set_kube_domain() {
    kubectl config use-context 
}

function deploy() {

  DATE=$(date +%s)
  helm upgrade \
  --wait \
  --install \
  --set environment=qa \
  --set image.tag=${CI_APPLICATION_TAG} \
  --namespace=${TENANT_NAME}-qa \
  --values ${NS_ENV}/values.yaml \
  ${APP_NAME}-qa \
  ${APP_FOLDER}/
  
}