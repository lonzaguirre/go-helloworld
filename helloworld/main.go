package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strings"
)

func display(w http.ResponseWriter, r *http.Request) {
	r.ParseForm() //Parse url parameters passed, then parse the response packet for the POST body (request body)
	// attention: If you do not call ParseForm method, the following data can not be obtained form
	fmt.Println(r.Form) // print information on server side.
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	r.ParseForm()

	fmt.Fprintf(w, "  # # ### #    #    ###  # # # ### ###  #   ### #\n")
	fmt.Fprintf(w, "  ### ##  #    #    # #  # # # # # # #  #   # # #\n")
	fmt.Fprintf(w, "  # # ### ###  ###  ###  ##### ### #  # ### ### .\n")
	fmt.Fprintf(w, "Hello %v %v!", r.FormValue("firstname"), r.FormValue("lastname")) // write data to response
}

func reg(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method) //get request method
	if r.Method == "GET" {
		t, _ := template.ParseFiles("login.gtpl")
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		// logic part of log in
		fmt.Println("firstname:", r.Form["firstname"])
		fmt.Println("lastname:", r.Form["lastname"])
	}
}

func main() {
	http.HandleFunc("/", reg)
	http.HandleFunc("/result", display) // setting router rule

	err := http.ListenAndServe(":8080", nil) // setting listening port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
